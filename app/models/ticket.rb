class Ticket < ActiveRecord::Base
  attr_accessible :name, :price, :quantity, :subtotal

  before_save do
    subtotal
  end
  def subtotal
    self.subtotal= self.quantity * self.price
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Menu.delete_all

Menu.create([{name:'Pasta', description: 'Comes with Sauce', price: '10'},
             {name:'Wings', description: 'Flavor of Choice', price: '15'},
             {name:'Burger', description: 'Any toppings', price: '12'}])